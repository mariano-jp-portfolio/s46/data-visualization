import React, { useState } from 'react';
import toNum from '../../helpers/toNum';

import { Doughnut } from 'react-chartjs-2';

export default function TopTen({ data }) {
	// console.log(data);
	
	const countriesStats = data.countries_stat;
	
	const countriesSanitized = countriesStats.map((country) => {
		const sanitized = {
			name: country.country_name,
			cases: toNum(country.cases)
		};
		
		return sanitized;
	});
	
	const topTenCountries = countriesSanitized.slice(0, 10);
	
	const labels = topTenCountries.map((country) => {
		return country.name;
	});
	
	const cases = topTenCountries.map((country) => {
		return country.cases;
	});
	// console.log(cases);
	
	return (
		<React.Fragment>
			<h1>10 Countries With The Highest Number of Cases</h1>
			<Doughnut
				data={{
					datasets: [{
						data: cases,
						backgroundColor: ["red", "orange", "yellow", "blue", "green", "indigo", "violet", "pink", "gray", "brown"]
					}],
					labels: labels
				}}
				redraw={false}
			/>
		</React.Fragment>
	);
};

// fetching data
export async function getStaticProps() {
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	});
	
	const data = await res.json();
	
	return {
		props: {
			data
		}
	}
};