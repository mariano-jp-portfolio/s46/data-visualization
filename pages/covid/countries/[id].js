import React from 'react';
import Banner from '../../../components/Banner';

// Next.js will create several pages for us that is associated with their own url's
export default function index({ country }) {
	return (
		<React.Fragment>
			<Banner
				country={country.country_name}
				deaths={country.deaths}
				criticals={country.serious_critical}
				recoveries={country.total_recovered}
			/>
		</React.Fragment>
	);
}

// will create the url's based on our data
// will return "/covid/countries/USA"
export async function getStaticPaths() {
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	});
	
	const data = await res.json();
	
	const paths = data.countries_stat.map(country => ({
		params: { id: country.country_name }
	}));
	
	return {paths, fallback: false}
};

// to fetch data
export async function getStaticProps({params}) {
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	});
	
	const data = await res.json();
	
 	const country = data.countries_stat.find(country => country.country_name === params.id);
	
	return {
		props: {
			country
		}
	}
};