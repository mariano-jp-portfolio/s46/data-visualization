import React, { useState } from 'react';
import { Alert, Form, Button } from 'react-bootstrap';
import toNum from '../../helpers/toNum';

import DoughnutChart from '../../components/DoughnutChart';

export default function Search({ data }) {
	// console.log(data);
	
	const countriesStats = data.countries_stat;
	const [targetCountry, setTargetCountry] = useState('');
	const [name, setName] = useState('');
	const [criticals, setCriticals] = useState(0);
	const [deaths, setDeaths] = useState(0);
	const [recoveries, setRecoveries] = useState(0);
	
	function searchCountry(e) {
		// prevents page redirection and loss of data
		e.preventDefault();
		
		const match = countriesStats.find(country => 
			country.country_name.toLowerCase() === targetCountry.toLowerCase());
		
		// console.log(match);
		if (match) {
			setName(match.country_name);
			setCriticals(toNum(match.serious_critical));
			setDeaths(toNum(match.deaths));
			setRecoveries(toNum(match.total_recovered));
		} else {
			setName('');
			setCriticals(0)
			setDeaths(0);
			setRecoveries(0);
		}
	}
	
	function clear(){
		// e.preventDefault();
		
		setTargetCountry('');
		setName('');
		setCriticals(0)
		setDeaths(0);
		setRecoveries(0);
	}
	
	return (
		<React.Fragment>
			<Form onSubmit={searchCountry}>
				<Form.Group controlId="country">
					<Form.Label><h2>Search for a country:</h2></Form.Label>
					<Form.Control 
						type="text"  
						value={targetCountry} 
						onChange={e => setTargetCountry(e.target.value)}
					/>
					<Form.Text>
						Get Covid-19 stats of a specific country.
					</Form.Text>
				</Form.Group>
				
				<Button variant="primary" type="submit">
					Submit
				</Button>
				<span>
					<Button className="mx-5" variant="danger" onClick={clear}>
						Clear
					</Button>
				</span>
			</Form>
			<hr />
			{name !== '' ?
				<React.Fragment>
					<h3>Country: {name}</h3>
					<DoughnutChart
						criticals={criticals}
						deaths={deaths}
						recoveries={recoveries}
					 />
				</React.Fragment>
				:
				<Alert variant="light" className="mt-4">
					Search for a country to visualize it's data.
				</Alert>
			}
		</React.Fragment>
	);
}

// Fetching data
export async function getStaticProps() {
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	});
	
	const data = await res.json();
	
	return {
		props: {
			data
		}
	}
};