import React from 'react';

import '../styles/globals.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

import Navibar from '../components/Navibar';

function MyApp({ Component, pageProps }) {
  return (
  	<React.Fragment>
  		<Navibar />
  		<Container className="mt-3">
  			<Component {...pageProps} />
  		</Container>
  	</React.Fragment>
  )
}

export default MyApp;