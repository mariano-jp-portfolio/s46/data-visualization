import React from 'react';
import Head from 'next/head';
import { Alert } from 'react-bootstrap'
import styles from '../styles/Home.module.css';

import toNum from '../helpers/toNum';

export default function Home({ globalTotal }) {
	// console.log(globalTotal);
	
	return (
		<React.Fragment>
			<Alert variant="info">
				<h1>Total Covid-19 cases in the world: <strong>{ globalTotal.cases }</strong></h1>
			</Alert>
		</React.Fragment>
	);
};

// Next.js Data Fetching (SSR)
export async function getServerSideProps() {
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	});
	
	const data = await res.json();
	const countriesStats = data.countries_stat;
	
	// storage for total number of cases
	let total = 0;
	
	countriesStats.forEach(country => {
		// helper function toNum is invoked to convert strings to numbers
		total += toNum(country.cases)
	});
	
	const globalTotal = {
		cases: total
	}
	
	return {
		props: {
			globalTotal
		}
	}
};