// Helper function to convert string to a number
const toNum = (str) => {
	// spread the string in an array
	const arr = [...str];
	/*"4,100"
	["4", ",", "1", "0", "0"]*/
	
	// filters out the commas in the array
	const filteredArr = arr.filter(element => element !== ',');
	/*["4", "1", "0", "0"]*/
	
	// convert back into a string via reduce and use parseInt to convert to a number
	return parseInt(filteredArr.reduce((x, y) => x + y));
	/*"4100" --> 4100*/
};

export default toNum;