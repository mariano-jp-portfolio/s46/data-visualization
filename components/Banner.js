// Template for our countries pages
import Link from 'next/link';
import { Jumbotron, Button } from 'react-bootstrap';

export default function Banner({ country, criticals, deaths, recoveries }) {
	return (
		<Jumbotron>
			<h1>{country}</h1>
			<p>Deaths: {deaths}</p>
			<p>Recoveries: {recoveries}</p>
			<p>Critical: {criticals}</p>
			<Button variant="secondary">
				<Link href="/covid/countries">
					<a>View Countries</a>
				</Link>
			</Button>
		</Jumbotron>
	);
};